const express = require("express");
const proxy = require("http-proxy-middleware");

const app = express();
const host = process.env.HOST;

app.use(express.static(__dirname + "/"));
app.use("/api", proxy({
  target: `http://${host}:8888`,
  changeOrigin: true
}));
app.use(/^\/(session|irma).*$/, proxy({
  target: `http://${host}:8088`,
  changeOrigin: true
}));

app.listen(3000);
