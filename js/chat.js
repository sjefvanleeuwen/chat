const HOST = window.location.hostname;
const IRMA_API_SERVER_ADDRESS = `http://${HOST}:3000`;
const REST_API_SERVER_ADDRESS = `http://${HOST}:3000/api`;
const chatWindow = new Bubbles(document.getElementById("chat"), "chatWindow");
let workflow = {};
let turn = true;

const translations = {
  "pbdf.pbdf.ageLimits.over18": ["🧑 ja", "🧒 nee"],
  "pbdf.nijmegen.address.houseNumber": ["🏠 Huisnummer delen"]
};

const init = function () {
  chatWindow.talk({ice: {says: [
    "Hallo!",
    "Afhankelijk van uw situatie kunt u voor regelingen van de gemeente in aanmerking komen."
  ]}});
  setTimeout(() => createWorkflow(), 5000);
};

const createWorkflow = function () {
  fetch(`${REST_API_SERVER_ADDRESS}/workflows`, {
    credentials: "same-origin",
    method: "POST",
    body: "",
    headers: new Headers({
      "Content-Type": "application/json"
    })
  })
  .then(response => response.json())
  .then(data => {
    workflow = data;
    checkWorkflow(workflow.id);
  })
  .catch(error => alert(error));
};

const checkWorkflow = function (workflowId) {
  fetch(REST_API_SERVER_ADDRESS + "/workflows/" + workflowId)
  .then(response => response.json())
  .then(data => {
    const previousResource = workflow.variables && workflow.variables.resource;
    const workflowVariables = data.variables || {};
    const currentResource = workflowVariables && workflowVariables.resource;
    workflow = data;

    if (workflowVariables.done) {
      return chatWindow.talk({ice: {says: [workflowVariables.explanation]}});
    }

    if ((! turn) || (workflow.state !== "READY") || (previousResource === currentResource)) {
      return pollWorkflow(workflowId);
    }

    chatWindow.talk({ice: {
      says: [workflowVariables.explanation],
      reply: translations[workflowVariables.attribute].map(reply => {
        return {
          question: reply,
          answer: "verify"
        };
      })
    }});
    turn = false;
  })
  .catch(error => {
    pollWorkflow(workflowId);
  });
};

const pollWorkflow = function (workflowId) {
  setTimeout(() => checkWorkflow(workflowId), 1000);
};

function updateWorkflow(value) {
  const payload = {};
  payload[workflow.variables.field] = value;
  fetch(`${REST_API_SERVER_ADDRESS}/workflows/${workflow.id}${workflow.variables.resource}`, {
    credentials: "same-origin",
    method: "POST",
    body: JSON.stringify(payload),
    headers: new Headers({
      "Content-Type": "application/json"
    })
  }).then(() => pollWorkflow(workflow.id));
}

/*
 * Has to global for chat bubble to work
 */
verify = function() {
  verifyAttribute(workflow.variables.attribute);
};

const verifyAttribute = function(attribute) {
  const server = IRMA_API_SERVER_ADDRESS;
  doSession(server, {
    type: "disclosing",
    content: [{
      label: "IRMA attribute",
      attributes: [attribute]
    }]
  }).then(function(result) {
    turn = true;
    workflow.state = "BUSY";
    updateWorkflow(result.disclosed[0].rawvalue);
  });
};

init();

function doSession(server, request) {
  return irma.startSession(server, request, 'none')
    .then(function(pkg) { return irma.handleSession(pkg.sessionPtr, {server: server, token: pkg.token, method: 'popup', language: 'en'}); })
    .catch(function(error) { console.log(error) });
}
