# Chat

The chat front-end of the Zeebe Conversation application. Limited proof-of-concept meant to run on local development environment.

## Requirements

 - Zeebe Conversation REST API accessible on http://localhost:8888
 - [irma-cli](https://irma.app/docs/next/irma-cli/) installed 

## Getting started

### Clone this repository
```
$ git clone --recursive <repo-url>
```
or
```
$ git clone <repo-url>
$ cd chat
$ git submodule update --init
```

### Install dependencies
```
$ npm install
$ git submodule foreach 'npm install && npm run build'
```
## Run the example IRMA API server
```
$ irma server --static-path utils/irmajs/examples/browser/
```

## Running the chat
```
$ HOST=<local network ip> node index.js
$ open http://<local network ip>:3000
```

